set nocompatible              " be iMproved
filetype off                  " required!

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle
" required! 
Plugin 'gmarik/Vundle.vim'


" My bundles here:
"
" original repos on GitHub
"
Plugin 'jlanzarotta/bufexplorer'
Plugin 'tpope/vim-fugitive'
Plugin 'tomasr/molokai'
Plugin 'altercation/vim-colors-solarized'
Plugin 'kien/ctrlp.vim'
Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
Plugin 'Lokaltog/vim-easymotion'
Plugin 'klen/python-mode'
Plugin 'davidhalter/jedi-vim'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'kshenoy/vim-signature'
"Plugin 'Yggdroot/indentLine'
Plugin 'spolu/dwm.vim'
Plugin 'mattn/emmet-vim'
Plugin 'Valloric/MatchTagAlways'


" vim-scripts repos
"
" Example:
" Bundle 'L9'

" non-GitHub repos
"
" Example:
" Bundle 'git://git.wincent.com/command-t.git'


" Git repos on your local machine (i.e. when working on your own plugin)
"
" Example
" Bundle 'file:///Users/gmarik/path/to/plugin'

call vundle#end()

filetype plugin indent on     " required!
"
" Brief help
" :BundleList          - list configured bundles
" :BundleInstall(!)    - install (update) bundles
" :BundleSearch(!) foo - search (or refresh cache first) for foo
" :BundleClean(!)      - confirm (or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Bundle commands are not allowed.
"

" =============================================================================
"
"                            Personal Vim Settings
" -----------------------------------------------------------------------------
"
" =============================================================================

"Auto vim reload
autocmd! bufwritepost .vimrc source %

" Enable Syntax and molokai
syntax enable
set background=dark
set t_Co=256
colorscheme molokai
let g:molokai_original = 1
set foldmethod=indent

" Map Leader Key
let mapleader=','

" Easier splits
map <c-h> <c-w>h
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l

" Easier Split resizes
map <leader>- <esc>:res -5<cr>
map <leader>= <esc>:res +5<cr>

map <leader>, <esc>:vertical resize -5<cr>
map <leader>. <esc>:vertical resize +5<cr>

" Easier tab Movements
map <leader>tn <esc>:tabnext<cr>
map <leader>tp <esc>:tabprevious<cr>

" Move down one line only
map j gj
map k gk

" Better Indentation
vnoremap < <gv
vnoremap > >gv

" Setup line numbers
" Setup line wrap
set nu
set tw=79

" Do not auto wrap text on load
set nowrap

" Do not auto wrap text while typing
set fo-=t

" Setup text bar
set colorcolumn=80
highlight ColorColumn ctermbg=7

" Setup Number column and background
highlight LineNr ctermfg=7
highlight LineNr ctermbg=235
highlight Normal ctermbg=234

" Setup dark cursor
set cursorline
hi CursorLine ctermbg=0 "8 = dark gray, 15 = white
hi Cursor ctermbg=15 ctermfg=8


" Powerline setup
set guifont=Anonymouse\ Pro\ for\ Powerline
set laststatus=2

" ctrlP setup
noremap <leader>p <esc>:CtrlP<cr>
let g:ctrlp_max_height=30
let g:ctrlp_working_path_mode = 'c'
let g:ctrlp_custom_ignore = {
\ 'dir':  '\v[\/]\.(git|hg|svn)$',
\ 'file': '\v\.(dmg|so|app|pyc)$',
\ 'link': 'some_bad_symbolic_links',
\ }
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*/venv/*
set wildignore+=*.pyc
set wildignore+=venv/*

" Python-mode
" Activate rope
" Keys:
" K             Show python docs
"   Rope autocomplete
" g     Rope goto definition
" d     Rope show documentation
" f     Rope find occurrences
" b     Set, unset breakpoint (g:pymode_breakpoint enabled)
" [[            Jump on previous class or function (normal, visual, operator modes)
" ]]            Jump on next class or function (normal, visual, operator modes)
" [M            Jump on previous class or method (normal, visual, operator modes)
" ]M            Jump on next class or method (normal, visual, operator modes)
let g:pymode_rope = 0

" Documentation
let g:pymode_doc = 1
let g:pymode_doc_key = 'K'

"Linting
let g:pymode_lint = 1
let g:pymode_lint_checker = "pyflakes,pep8"
" Auto check on save
let g:pymode_lint_write = 1

" Support virtualenv
let g:pymode_virtualenv = 1

" Enable breakpoints plugin
let g:pymode_breakpoint = 1
let g:pymode_breakpoint_key = 'b'

" syntax highlighting
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all

" Don't autofold code
let g:pymode_folding = 0


" Jedi Vim doc
"Completion <C-Space>
"Goto assignments <leader>g (typical goto function)
"Goto definitions <leader>d (follow identifier as far as possible, includes imports and statements)
"Show Documentation/Pydoc K (shows a popup with assignments)
"Renaming <leader>r
"Usages <leader>n (shows all the usages of a name)
"Open module, e.g. :Pyimport os (opens the os module)
" Use l to toggle display of whitespace
nmap <leader>l :set list!
" And set some nice chars to do it with
set listchars=tab:>-

" automatically change window's cwd to file's dir
set autochdir

" I'm prefer spaces to tabs
set tabstop=4
set shiftwidth=4
set expandtab

" more subtle popup colors
if has ('gui_running')
highlight Pmenu guibg=#cccccc gui=bold
endif

" NerdTree
map <leader>o <esc>:NERDTreeToggle<cr>
let NERDTreeIgnore = ['\.pyc$']

" indentLines Settings

let g:indentLine_color_term = 239

" Powerline Stuff
let g:Powerline_symbols = 'unicode'
set laststatus=2
set encoding=utf-8
set t_Co=256
set fillchars+=stl:\ ,stlnc:\

" Jedi Key Bindings
let g:jedi#documentation_command = "<C-k>"
let g:jedi#completions_command = "<C-j>"

" Emmet
let g:user_emmet_leader_key='<C-e>'

